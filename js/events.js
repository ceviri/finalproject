"use strict";

class Event {
	constructor(title, date, location, time, description) {
		this.title = title;
		this.date = date; // JS Date() object
		this.location = location;
		this.time = time; // String
		this.description = description;
		this.expanded = false;
	}

	render() {
		return `
            <div class="calendar-event">
                <div class="calendar-event-main-info">
                    <div class="calendar-event-title">${this.title}</div>
                    <div class="calendar-event-table">
                        <div class="row">
                            <div class="col-6 col-xl-3 contact-table-label">Location:</div>
                            <div class="col-6 col-xl-9">${this.location}</div>
                        </div>
                        <div class="row">
                            <div class="col-6 col-xl-3 contact-table-label">Time:</div>
                            <div class="col-6 col-xl-9">${this.time}</div>
                        </div>
                    </div>
                </div>
                <div class="calendar-event-description">${this.description}</div>
            </div>
		`
	}

	isOnDate(d) {
		return d.getDate() === this.date.getDate()
			&& d.getMonth() === this.date.getMonth()
			&& d.getYear() === this.date.getYear();
	}
}


let activeMonth = new Date(Date.now());
let activeDate = new Date(Date.now());

let events = [
	new Event(
		"Field Day",
		new Date(Date.now()),
		"Dan Marino Field Food Bank",
		"12pm",
		`Help yourself to a free light snack on the 2nd floor – toast, butter/jelly, hard boiled eggs, fresh fruit, coffee/tea, plenty of water to stay hydrated!

  		Games, coloring books, puzzles are available as well. Enjoy our spacious deck and beautiful planters, too!`
	),
	new Event(
		"Pool Tournament",
		new Date(Date.now()),
		"Games Room",
		"8pm",
		`At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio.

        Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.`
	),
	new Event(
		"Gala Watch Party",
		new Date(Date.now()),
		"Lobby",
		"6pm",
		`Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.

        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.`
	)
]

let placeholderEventGenerator = () => {
	let newDate = new Date(Date.now());
	newDate.setDate(Math.floor(Math.random() * 60))

	let bignumber = 500;
	let loremipsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.`;
	let li_words = loremipsum.split(" ");
	let a = Math.floor(Math.random() * li_words.length);
	let b = Math.floor(Math.random() * (li_words.length - a)) + a;
	let description = li_words.slice(a, b).join(" ")
	return new Event(
		`Placeholder Event ${Math.floor(Math.random() * bignumber)}`,
		newDate,
		`Placeholder Location ${Math.floor(Math.random() * bignumber)}`,
		`Noon`,
		description
	)
}

for (let i = 0; i < 40; i++) {
	events.push(placeholderEventGenerator());
}

let updateCalendar = () => {
	activeMonth.setDate(1);
	let counter = new Date(activeMonth);
	let output = "";
	let weeks = [];
	let thisWeek = [];
	/* This parses the days of the displayed month and returns data. The first entry is
	 * the day of the month, the second is a css class that styles the cell depending on whether
	 * it needs to be displayed, or appear to be active or inactive.
	 */
	while (counter.getMonth() === activeMonth.getMonth()) {
		if (counter.getDay() === 0) {
			if (thisWeek.length > 0)
				weeks.push(thisWeek);
			thisWeek = [];
		}

		if (thisWeek.length === 0) {
			// Pads the start of the week with empty days
			for (let i = 0; i < counter.getDay(); i++) {
				thisWeek.push(["", " empty"])
			}
		}
		let hasEvent = events.some(e => e.isOnDate(counter));
		let cssClass = hasEvent ? "" : " inactive";
		if (activeDate.getDate() === counter.getDate()
		 	&& activeDate.getMonth() === counter.getMonth()
		 	&& activeDate.getYear() === counter.getYear()) {
			cssClass = " current";
		}
		thisWeek.push([counter.getDate(), cssClass])
		counter = new Date(counter);
		counter.setDate(counter.getDate() + 1);
	}
	// Pads the end of the week with empty days
	let padDays = counter.getDay() === 0 ? 0 : 7 - counter.getDay();
	for (let i = 0; i < padDays; i++) {
		thisWeek.push(["", " empty"])
	}
	weeks.push(thisWeek);

	// Renders a single week
	let weekRenderer = week => `<div class="calendar-row">
    	${week.map(data =>  {
    		let onClick = data[0] === "" ? "" : ` onclick="setDay(${data[0]})"`;
	    	return `<div class="calendar-date ${data[1]}"${onClick}>${data[0]}</div>`;
    	}).reduce((a, b) => a + b, "")}
	</div>`

	// Rernders the whole calendar
	document.getElementById("calendar-body").innerHTML = weeks.map(weekRenderer).reduce((a, b) => a + b, "");

	// Updates the date
	document.getElementById("month-text").innerHTML = Intl.DateTimeFormat('en-US', {month: "long"}).format(activeMonth);
	document.getElementById("year-text").innerHTML = activeMonth.getYear() + 1900;
}

let updateEventsList = () => {
	let visibleEvents = events.filter(e => e.isOnDate(activeDate));
	document.getElementById("calendar-events-list").innerHTML = `
	    ${visibleEvents.map(e => e.render()).reduce((a, b) => a + b, "")}
	`
	document.getElementById("calendar-events-date").innerHTML = Intl.DateTimeFormat('en-US', {month: "long", day: "numeric"}).format(activeDate);
}

let setDay = day => {
	activeDate = new Date(activeMonth);
	activeDate.setDate(day);
	updateCalendar();
	updateEventsList();
}

let incrementMonth = () => {
	activeMonth.setMonth(activeMonth.getMonth() + 1);
	updateCalendar();
}

let decrementMonth = () => {
	activeMonth.setMonth(activeMonth.getMonth() - 1);
	updateCalendar();
}

updateCalendar();
updateEventsList();