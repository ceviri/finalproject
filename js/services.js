"use strict";

let sections = [
	"Services",
	"Wellness",
	"Social and Rehabilitation",
	"Employment and Education",
	"Peer Support"
]

let texts = [
	`Peoples Oakland is a premier recovery and wellness center for people living with serious, persistent mental illness and co-occurring disorders. Recovery programs are holistic, comprehensive, and member driven. To support innovative approaches to mental health the center includes a multipurpose social and drop-in area, a commercial kitchen, a fitness center, meeting rooms, a secure outside deck and a resource and computer center. All members have an individual counselor and develop a recovery plan based on their goals as they define them.

	Programs include nutrition and fitness, employment and education, peer support, social and recreational activities in the center and in the community. Fun and social connectedness is very important.`,

	`Wellness and Fitness Activities assist members in understanding and applying a holistic approach to creating and maintaining a healthy lifestyle. A wide range of activities are available. Members work with staff to develop and follow their individualized wellness recovery plans.

	In the fitness center members use aerobic and weight training equipment and participate in classes such as yoga, walking groups, weight management and smoking cessation. Nutritional education is incorporated through the healthy foods offered at the Snack Shack and every event, celebration and function. Nutritional health is also a focus in the diabetes education class and weight management class.`,
	
	`Social and Rehabilitation activities provide opportunities to meet and make friends and to participate in fun events at Peoples Oakland and in the community. Activities are continually evolving to meet the interests of the membership.

	Events range from parties, cook-outs and playing pool to day outings and overnight vacations. There is an all season sports program with competition against other agencies in basketball, softball and volleyball.`,

	`Peoples Oakland offers a wide range of employment services and education supports. Supports and opportunities are tailored to each member’s strength and interests. Member’s interest range from entering the work force to pursuing their education by attending local training centers and universities. Services offered include: individual career counseling; on the job training opportunities in reception, clerical support, housekeeping; job skills classes; job research assistance; job coaching and support; educational supports and individual computer accounts with internet access.

	Peoples Oakland is committed to assisting individuals with mental illness to find and keep employment in the community. The Employment Assistance Program (EAP) at Peoples Oakland emphasizes helping them obtain competitive work in the community and providing the supports necessary to ensure their success in the workplace. We have staff that can help you with all phases of employment. Services are customized to the needs and desires of the individuals served. These services may include: benefits counseling, career counseling, resume development, interview practice, job development, placement and assistance in keeping a job. Staff is also available to assist you with information and support in your training and/or educational pursuits.`,

	`Peoples Oakland has always been a leader in self-help and peer support models. Opportunities are available for members to develop, lead and participate in activities and groups; work as a peer counselor; participate in community consumer activities such as CSP and PSAN; attend conferences and workshop. In addition, informal social relationships with peers allow unlimited opportunities to both provide and receive support.`,
]

let images = [
	"img/Services.jpeg",
	"img/Wellness.jpeg",
	"img/Rehabilitation.jpeg",
	"img/Employment.jpeg",
	"img/Peer.jpeg"
]

let contentGen = new ContentGenerator(sections, texts, images);
let onButton = i => contentGen.onButton(i);

contentGen.onLoad();