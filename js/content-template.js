"use strict";

class ContentGenerator {
	constructor (sections, texts, images) {
		this.sections = sections;
		this.texts = texts;
		this.images = images;
		this.activeIndex = 0;
	}

	update() {
		document.getElementById("article-image-section").innerHTML = `
	        <img class="home-section-image" src="${this.images[this.activeIndex]}" />
		`
		let buttonMap = index =>
			`<div class="article-section-button${index === this.activeIndex ? ' active' : ''}" onclick="onButton(${index})">${this.sections[index]}</div>`;
		document.getElementById('article-button-section').innerHTML = `
	        <div class="article-section-button-container">
	        	${this.sections.map((s, i) => buttonMap(i)).reduce((a, b) => a + b, "")}
	        </div>
	        <div class="separator d-none d-xl-block"></div>
		`
		document.getElementById("article-text-section").innerHTML = `
	        <div class="article-text-container">${this.texts[this.activeIndex]}</div>
		`

	}

	generateFullText() {
		let fetchImageDiv = index => `<div class="article-image-div">
	            <div class="article-title-container">${this.sections[index]}</div>
	            <img class="article-image" src="${this.images[index]}" />
	        </div>
		`
		let fetchSection = index => fetchImageDiv(index) + this.texts[index] + "<br> <br>";
		document.getElementById("article-full-text-section").innerHTML = `
	        <div class="article-text-container">${this.sections.map((s, i) => fetchSection(i)).reduce((a, b) => a + b, "")}</div>
	    `
	}

	scrollFullText(index) {
		let element = document.querySelectorAll(`#article-full-text-section .article-image-div`)[index];
		element.scrollIntoView({behavior: "smooth"});
	}

	onLoad() {
		this.update();
		this.generateFullText();
	}

	onButton(i) {
		this.activeIndex = i;
		this.update();
		this.scrollFullText(i);
	}
}