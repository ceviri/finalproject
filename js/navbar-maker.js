"use strict";

let navSections = [
	"About Us",
	"Services",
	"Events",
	"Community Resources",
    "Contact Us"
]

let hrefs = [
	"about-us.html",
	"services.html",
	"events.html",
	"community-resources.html",
    "contact-us.html"
]

let generateNavLink = (extras="") => (index) => `<a class="nav-link text-center${extras}" href="${hrefs[index]}">${navSections[index]}</a>`;

let generateNavItem = index => `
    <li class="nav-item">
        ${generateNavLink()(index)}
    </li>
`

let generateNavs = () => {
	document.getElementById("navbarSupportedContent").innerHTML = `
        <ul class="navbar-nav mr-auto ml-2">
        	${navSections.map((s, i) => generateNavItem(i)).reduce((a, b) => a + b, "")}
        </ul>
	`
	document.getElementById("navbar").innerHTML = `
        <div class="navbar-brand">
            <object data="img/mini_logo_svg.svg" alt="Peoples Oakland Icon" class="d-xl-none nav-icon-small" type="image/svg+xml"></object>
            <img src="img/logo-large.png" alt="Peoples Oakland Logo" class="d-none d-xl-block nav-icon-large">
            <a class="navbar-clickable" href="index.html"></a>
        </div>
        <ul class="d-none d-xl-flex nav">
        	${navSections.map((s, i) => generateNavItem(i)).reduce((a, b) => a + b, "")}
        </ul>
        <button class="navbar-toggler header-button" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span> Menu </span>
        </button>
        <div class="right-fixed-container">
            <li class="nav-item header-button">
                <a class="nav-link" href="https://www.paypal.com/">Donate</a>
            </li>
            <div class="fb-button">
                <a href="https://www.facebook.com/peoplesoakland/"></a>
                <object data="img/fb.svg" alt="Peoples Oakland Facebook"></object>
           </div>
        </div>
	`
}

generateNavs();