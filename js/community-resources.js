"use strict";

let categories = [
	"Oakland Community Organizations",
	"Other Area Community Organizations",
	"Local and Statewide Information",
	"National Information",
	"Additional Information"
]

let links = [
	[
		[`Community Human Services`, "https://www.chscorp.org"], 
		["Oakland Planning and Development Corporation", "https://www.opdc.org"], 
		['Oakland Transportation Management Association', "http://www.otma-pgh.org"]
	],
	[
		["Bethlehem Haven","https://bethlehemhaven.org"],
		["Bidwell Training Center","https://bidwelltraining.edu"],
		["Career Link – Pittsburgh","https://www.careerlinkpittsburgh.com"],
		["Center for Disability Law and Policy","https://www.equalemployment.org"],
		["Goodwill Industries of Pittsburgh","http://www.goodwillpitt.org"],
		["Greater Pittsburgh Community Food Bank","https://pittsburghfoodbank.org"],
		["Just Harvest","https://justharvest.org"],
		["Hill House Association","http://www.hillhouse.org"],
		["Peer Support and Advocacy Network","http://www.peer-support.org"]
	],
	[
		["Allegheny County Department of Human Services","https://www.alleghenycounty.us/human-services/index.aspx"],
		["Allegheny County Department of Human Services – Housing/Homeless Resource Guide","https://peoplesoakland.org/Links/Housing%20Resource%20Guide.pdf"],
		["Allegheny County Health Department","http://www.achd.net/mainstart.html"],
		["Allegheny County Office of Behavioral Health","https://www.alleghenycounty.us"],
		["City of Pittsburgh","https://pittsburghpa.gov"],
		["Disability Rights Network of Pennsylvania","https://www.disabilityrightspa.org"],
		["Port Authority","https://www.portauthority.org"],
		["Pennsylvania Mental Health Consumers Association","https://pmhca.wildapricot.org"]
	],
	[
		["United States Psychiatric Rehabilitation Association","http://www.psychrehabassociation.org"],
		["NAMI, The National Voice for Mental Illness","https://www.nami.org"],
		["Substance Abuse and Mental Health Services Administration","https://www.samhsa.gov"],
		["Boston University Center for Psychiatric Rehabilitation","https://cpr.bu.edu"],
		["Job Accommodation Network","http://janweb.icdi.wvu.edu"],
		["Bazelon Center for Mental Health Law","http://www.bazelon.org"],
		["National Institute of Mental Health","https://www.nimh.nih.gov/index.shtml"],
		["National Mental Health Consumer’s Self-Help Clearinghouse","http://www.mhselfhelp.org"],
		["National Empowerment Center","http://www.power2u.org"],
		["SoberRecovery.Com Thousands of Web Resources for Substance Abuse and Mental Health Recovery","http://www.soberrecovery.com/links/resources.html"]
	],
	[
		["Financial Help for the Disabled","https://www.incharge.org/debt-relief/financial-help-disabled"],
		["How to Get a Service Dog: A Complete Guide","https://gallant.com/blog/how-to-get-a-service-dog"],
		["Rental Assistance","https://www.peoplesoakland.org/wp-content/uploads/2021/03/Rental-Assistance-flyer.pdf"],
		["Rental Assistance Drop-In Centers","https://www.peoplesoakland.org/wp-content/uploads/2021/03/rentReliefDropInCenters.pdf"],
		["Career Guide for People With Disabilities","https://novoresume.com/career-blog/disability-career-guide"],
		["17 Ways to Stay Sober When You’re Stuck at Home","https://www.thetemper.com/covid19-sobriety-tips/"]
	],
]

let sectionShown = categories.map(_ => false);

let resourcesClick = index => {
	let toggled = sectionShown[index];
	sectionShown[index] = !toggled;
	if (toggled) {
		document.querySelectorAll(".resources-section-header-subtitle")[index].classList.add("show");
		document.querySelectorAll(".resources-section-header-triangle")[index].classList.add("rotated");
	} else {
		document.querySelectorAll(".resources-section-header-subtitle")[index].classList.remove("show");
		document.querySelectorAll(".resources-section-header-triangle")[index].classList.remove("rotated");
	}
}

let generateLink = data => {
	const [title, href] = data;
	return `
	<div class="col-12 col-md-6 col-xl-4">
    	<a href="${href}">${title}</a>
    </div>`
}


let generateRow = index => {
	return `
        <div class="row d-md-none">
            <div class="col-11 resources-section-header" onclick="resourcesClick(${index})" data-target="#target${index}" data-toggle="collapse">
                <div class="resources-section-header-text">${categories[index]}</div>
                <div class="resources-section-header-subtitle collapse show">(click to expand)</div>
                <div class="resources-section-header-triangle rotated"></div>
            </div>
        </div>
        <div class="row d-none d-md-flex">
            <div class="col-11 resources-section-header">
                <div class="resources-section-header-text">${categories[index]}</div>
            </div>
        </div>
        <div class="row d-md-none collapse" id="target${index}">
            ${links[index].map(generateLink).reduce((a, b) => a + b, "")}
        </div>
        <div class="row d-none d-md-flex">
            ${links[index].map(generateLink).reduce((a, b) => a + b, "")}
        </div>
    `
}

let generateHTML = () => {
	document.getElementById("main-container").innerHTML = `
        <div class="page-title">Community Resources</div>
        ${links.map((_, i) => generateRow(i)).reduce((a, b) => a + b, "")}
	`
}
generateHTML();