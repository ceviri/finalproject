"use strict";

class ContactInformation {
	constructor(name, title, extension) {
		this.name = name;
		this.title = title;
		this.extension = extension;
	}

	render() {
		return `
            <div class="card contact-card">
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.title}</p>
                    <p class="card-text">Telephone Extension: <b class="card-text">${this.extension}</b></p>
                </div>
            </div>
		`
	}
}


let data = [
	new ContactInformation("Lezetta L. Cox", "Executive Director", "240"),
	new ContactInformation("Allison Haley, MSW, LSW", "Psychiatric Rehabilitation Supervisor", "259"),
	new ContactInformation("Jennie Pabst, MSW", "Referral and Intake Coordinator", "249"),
	new ContactInformation("Joel Heiney", "Resource Coordinator", "236"),
	new ContactInformation("Kiara Davis, MSW", "Psychiatric Rehabilitation Specialist", "225"),
	new ContactInformation("Jen Lallemand, MA", "Psychiatric Rehabilitation Specialist", "246"),
	new ContactInformation("Eric Lewis, MS", "Psychiatric Rehabilitation Specialist", "223"),
	new ContactInformation("Andrea Tempalski, MSW", "Psychiatric Rehabilitation Specialist", "237"),
	new ContactInformation("Wilma Sirmons, MSW", "Additional Staff", "233"),
]


let generateHTML = () => {
	document.getElementById("contact-card-container").innerHTML = `
        ${data.map(d => d.render()).reduce((a, b) => a + b, "")}
	`
}
generateHTML();