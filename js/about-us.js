"use strict";

let sections = [
	"About Us",
	"Our Mission",
	"Our History",
	"Jobs / Internships"
]

let texts = [
	`Peoples Oakland is a private non-profit agency established by local residents as a community, planning and organizing agency. Originally, the mission was to enhance the integrity of the community through communications and negotiation among residents, merchants and institutional representatives.

	Peoples Oakland will continue to build an organization that, with partners and through networks, will make this region a leader in supporting people who are recovering from serious and persistent mental illness and co-occurring disorders.`,

	`Peoples Oakland strives to be a premier recovery and wellness center, and is committed to holistic, comprehensive, member driven recovery. Guiding all Peoples Oakland services and activities is a philosophy of Recovery based on real life experiences nurtured by peer support, hope, self-help and collaborative relationships with professionals. Peoples Oakland continues as a leader in providing Recovery based comprehensive psychiatric and social rehabilitation services for Allegheny County adults with severe and persistent mental illness and co-occurring disorders.`,

	`The Peoples Oakland Mental Health Program was founded in response to the crisis faced by thousands of state mental hospital patients who, through deinstitutionalization programs, were released into the community with few supporters or social connections. Peoples Oakland began to organize former state hospital patients, providing education to support them in advocating for their rights.`,

	`Peoples Oakland is always looking for staff, volunteers and students who have the best “people skills” and who enjoy the challenges and rewards of working with our members. Paid positions include rehabilitation counselors, program supervisors and administrative and clerical positions.

	Peoples Oakland collaborates with all the area universities and colleges. We have placements available each term for students to complete their internships and/or practicum at both the undergraduate and graduate levels. We work with the various disciplines and departments such as social work, psychology, rehabilitation counseling, pharmacology and information technology. Placements are supervised by staff members based on the academic requirements.`,
]

let images = [
	"img/About.jpeg",
	"img/Mission.jpeg",
	"img/History.jpeg",
	"img/Intern.jpeg"
]

let contentGen = new ContentGenerator(sections, texts, images);
let onButton = i => contentGen.onButton(i);

contentGen.onLoad();