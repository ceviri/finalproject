"use strict";

let supporters = [
	"Allegheny HealthChoices",
	"NexTier Bank",
	"University of Pittsburgh",
	"UPMC",
	"UPMC Community Care"
]


document.getElementById("supporter-list").innerHTML = `
    ${supporters.map(n => `<div class="row">${n}</div>`).reduce((a, b) => a + b, "")}
`